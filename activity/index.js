console.log('Hello Radh!');
console.log(document.querySelector('#text-firstName'))

const textFirstName = document.querySelector("#text-firstName");
const textLastName = document.querySelector("#text-lastName");
const spanFullName = document.querySelector("#span-fullName");

// Event Listeners
// textFirstName.addEventListener('keyup', (event) => {
// 	spanFullName.innerHTML = textFirstName.value;
// 	console.log(event.target);
// 	console.log(event.target.value);
// })

// function printFirstName(event) {
// 	spanFullName.innerHTML = textFirstName.value;
// }

// textFirstName.addEventListener('keyup', printFirstName);

// const spanFirstName = document.querySelector("#span-firstName");

// spanFirstName.addEventListener("click", (event) => {
// 	alert("You clicked the First Name label!");
// })

function printFullName(event) {
	spanFullName.innerHTML = textFirstName.value + ' ' + textLastName.value;
}

textFirstName.addEventListener('keyup', printFullName);
textLastName.addEventListener('keyup', printFullName);